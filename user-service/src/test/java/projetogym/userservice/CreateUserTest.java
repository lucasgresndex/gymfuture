package projetogym.userservice;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import projetogym.userservice.dto.UserDTO;
import projetogym.userservice.models.User;
import projetogym.userservice.repositories.UserRepository;
import projetogym.userservice.service.UserService;

@ExtendWith(MockitoExtension.class)
public class CreateUserTest {
    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserService userService;

    @Test
    public void testCreateUser() {
        // Dados fictícios para o DTO do usuário
        UserDTO userDTO = new UserDTO();
        userDTO.setName("John");
        userDTO.setEmail("john@example.com");

        // Configurar o comportamento do mock do repositório
        User savedUser = new User();
        savedUser.setId(1L);
        savedUser.setName("John");
        savedUser.setEmail("john@example.com");
        when(userRepository.save(Mockito.any(User.class))).thenReturn(savedUser);

        // Chamar o método que será testado
        UserDTO result = userService.createUser(userDTO);

        // Verificar se o usuário foi salvo corretamente
        assertEquals(savedUser.getId(), result.getId());
        assertEquals(savedUser.getName(), result.getName());
        assertEquals(savedUser.getEmail(), result.getEmail());
    }
}
